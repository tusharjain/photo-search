import React from "react";
import { createApi } from "unsplash-js";

const MY_ACCESS_KEY = "CeW6Z73Vi_gqQc6sWbb2Q8GTnnNAlth1_ktrCWpxCrs";

const unsplash = createApi({
  accessKey: MY_ACCESS_KEY,
  // `fetch` options to be sent with every request
  headers: { "X-Custom-Header": "foo" },
});

export default class SearchPhotos extends React.Component {
  //   const [query, setQuery] = useState("");
  //   const [url, setUrl] = useState(null);
  constructor() {
    super();
    this.state = {
      query: "",
      url: [],
    };
    this.searchPhotos = this.searchPhotos.bind(this);
    this.setQuery = this.setQuery.bind(this);
  }

  searchPhotos = (e) => {
    e.preventDefault();

    unsplash.search
      .getPhotos({
        query: this.state.query,
        orientation: "portrait",
      })
      .then((data) => {
        this.setState(() => {
          let tempState = Object.assign({}, this.state);
          tempState.url = data.response.results.map(
            (imgObj) => imgObj.urls.thumb
          );
          console.log(tempState); 
          return tempState;
        });

        console.log(this.state.url);
      });

    
  };

  setQuery(e) {
    this.setState(() => {
      let query = e.target.value;
      let tempState = Object.assign({}, this.state);
      tempState.query = query;
      return tempState;
    });
  }

  render() {
    console.log('*');
    return (
      <>
        <form className="form" onSubmit={this.searchPhotos}>
          <label className="label" htmlFor="query">
            {" "}
            📷
          </label>
          <input
            type="text"
            name="query"
            className="input"
            placeholder={`Try "dog" or "apple"`}
            value={this.state.query}
            onChange={this.setQuery}
          />
          <button type="submit" className="button">
            Search
          </button>
        </form>
      </>
    );
  }
}
